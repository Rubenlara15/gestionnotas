/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestion;

import java.util.Scanner;
     

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public class GestionDam {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int opcion, contAlumnoIntroducido = 0, numeroAlumnos;
        Scanner input = new Scanner(System.in);
        System.out.println("Introduce el numero de alumnos del grupo");
        numeroAlumnos = input.nextInt();
        String [] listaAlumnos = Alumnos.inicializarLista(numeroAlumnos);
        int [][] listaNotas= Notas.inicializarNotas(numeroAlumnos);
        do {
            imprimirMenu();
            opcion = input.nextInt();
            switch(opcion){
                case 1:
                    listaAlumnos = Alumnos.aniadirAlumnos(contAlumnoIntroducido,listaAlumnos);
                    contAlumnoIntroducido++;
                    break;
                case 2:
                    Alumnos.listarAlumnos(listaAlumnos);
                    break;
                case 3:
                    listaNotas = Notas.aniadirNota(listaNotas);
                    break;
                case 4:
                    Notas.mostrarNotasAlumno(listaNotas);
                    break;
                case 5:
                    break;
                default:
                    System.out.println("Opción incorrecta, elija de nuevo");
                    break;
            }
        }while(opcion!=5);
    }

    private static void imprimirMenu() {
        System.out.println("Sistema de gestión de notas de DAM1");
        System.out.println("Elija una opción");
        System.out.println("1) Añadir alumno");
        System.out.println("2) Listar alumnos");
        System.out.println("3) Añadir nota alumno");
        System.out.println("4) Mostrar notas alumno");
        System.out.println("5) Salir");
    }
    
}
